package com.devedu.sql_task;

import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResultsHandler {

    private DBcontroller dataRes;
    private List<String> result;
    private final Logger log = Logger.getLogger(getClass());

    public ResultsHandler() {
        dataRes = new DBcontroller();
        result = new ArrayList<>();
        collectResults();
        dataRes.closeConnection();
    }

    private void printResult(String query) {
        ResultSet resultSet = dataRes.executeQuery(query);
        ResultSetMetaData rsmd = null;
        try {
            rsmd = resultSet.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            for (int i = 1; i <= columnsNumber; i++) {
                System.out.print(rsmd.getColumnName(i) + "\t");
            }
            System.out.println();
            while (resultSet.next()) {
                String row = "";
                for (int i = 1; i <= columnsNumber; i++) {
                    row += resultSet.getString(i) + "\t";
                }
                System.out.println(row);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }

    private void collectResults() {
        printTask1();
        printTask2();
        printTask3();
        printTask4();
        printTask5();
        printTask6();
        printTask7();
        printTask8();
        printTask9();
        printTask10();
    }

    private void printTask1() {
        System.out.println("============= Общее число жителей: =============");
        printResult("SELECT Count(p.id) as peopleCount FROM persons p");
    }

    private void printTask2() {
        System.out.println("============= Средний возраст жителей: =============");
        printResult("SELECT Avg(p.age) as avgAge FROM persons p");
    }

    private void printTask3() {
        System.out.println("============= Список фамилий без повторений: =============");
        printResult("SELECT DISTINCT p.\"lName\" FROM persons p ORDER BY p.\"lName\" ASC");
    }

    private void printTask4() {
        System.out.println("============= Список фамилий с количеством их повторений: =============");
        printResult("SELECT DISTINCT p.\"lName\", count(p.\"lName\") FROM persons p GROUP BY p.\"lName\" ORDER BY Count(p.\"lName\") DESC");
    }

    private void printTask5() {
        System.out.println("============= Список фамилий c буквой 'б' в середине: =============");
        printResult("SELECT p.\"lName\" FROM persons p WHERE p.\"lName\" LIKE '%б%' and p.\"lName\" NOT LIKE 'б%' and p.\"lName\" NOT LIKE '%б'");
    }

    private void printTask6() {
        System.out.println("============= Список людей у которых не определена улица: =============");
        printResult("SELECT p.\"id\", p.\"fName\", p.\"lName\", p.age FROM \"public\".persons p " +
                "LEFT JOIN \"public\".streets s on p.\"streetID\"=s.\"id\" WHERE s.\"id\" is null");
    }

    private void printTask7() {
        System.out.println("============= Список несовершеннолетних, проживающих на пр-те Победы: =============");
        printResult("SELECT p.\"id\", p.\"fName\", p.\"lName\", p.age FROM persons p" +
                " LEFT JOIN streets s on p.\"streetID\"=s.\"id\" WHERE p.age<18 and s.\"name\" LIKE '%Правды%'");
    }

    private void printTask8() {
        System.out.println("============= Упорядоченный по алфавиту список всех улиц с указанием, сколько жильцов живёт на улице: =============");
        printResult("SELECT s.\"name\", count(p.\"id\") FROM streets s" +
                " LEFT JOIN persons p on s.\"id\"=p.\"streetID\" GROUP BY s.\"name\"" +
                " ORDER BY count(p.\"id\") DESC");
    }

    private void printTask9() {
        System.out.println("============= Список улиц, название которых состоит из 12-ти букв: =============");
        printResult("SELECT s.\"name\" FROM streets s WHERE length(s.\"name\")=12");
    }

    private void printTask10() {
        System.out.println("============= Список улиц с количеством жильцов на них меньше 3: =============");
        printResult("SELECT s.\"name\", count(p.\"id\") FROM streets s " +
                "LEFT JOIN persons p on s.\"id\"=p.\"streetID\" GROUP BY s.\"name\" HAVING count(p.\"id\")<3");
    }


}
