package com.devedu.sql_task;

import org.apache.log4j.Logger;

public class Main {
    private final Logger log = Logger.getLogger(getClass());

    public static void main(String[] args) {
        new ResultsHandler();
    }
}
