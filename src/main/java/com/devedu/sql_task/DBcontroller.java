package com.devedu.sql_task;


import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBcontroller {
    private Connection con;
    private PreparedStatement pst;
    private ResultSet rs;
    private Properties prop;
    private final Logger log = Logger.getLogger(getClass());

    public DBcontroller() {
        prop = new Properties();
        getConnection();
    }

    private void getConnection() {
        try {
            con = DriverManager.getConnection(prop.URL, prop.USER, prop.PASSWORD);
            log.info("DB connected");
        } catch (SQLException exception) {
            log.error("DB connection error " + exception.getMessage());
        }
    }

    public ResultSet executeQuery(String query) {
        try {
            pst = con.prepareStatement(query);
            rs = pst.executeQuery();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return rs;
    }

    public void closeConnection() {
        try {
            rs.close();
            con.close();
            log.info("DB connection closed.");
        } catch (SQLException exception) {
            log.error(exception.getMessage());
        }
    }
}
