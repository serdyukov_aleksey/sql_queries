Задания по SQL

Даны две таблицы:
● Список улиц - Street (Id, Name)
● Cписок жильцов - Person (Id, FirstName, LastName, Age, Id_Street)
Задания:
1. Вывести общее число жителей
SELECT
Count(p.id)
FROM
persons p

2. Вывести средний возраст жителей
SELECT
Avg(p.age)
FROM
persons p

3. Вывести отсортированный по алфавиту список фамилий без повторений
SELECT DISTINCT
p."lName"
FROM
persons p
ORDER BY
p."lName" ASC

4. Вывести список фамилий, с указанием количества повторений этих фамилий в
общем списке
SELECT DISTINCT
p."lName",
Count(p."lName")
FROM
persons p
GROUP BY
p."lName"
ORDER BY
Count(p."lName") DESC

5. Вывести фамилии, которые содержат в середине букву «б»
SELECT
p."lName"
FROM
persons p
WHERE
p."lName" LIKE '%б%' and p."lName" NOT LIKE 'б%' and p."lName" NOT LIKE '%б'

6. Вывести список «бомжей»
SELECT 
p."id",
p."fName",
p."lName",
p.age
FROM
"public".persons p
LEFT JOIN
"public".streets s
on p."streetID"=s."id"
WHERE s."id" is null

7. Вывести список несовершеннолетних, проживающих на проспекте Правды
SELECT 
 p."id",
 p."fName",
 p."lName",
 p.age
 FROM
 persons p
 LEFT JOIN
 streets s
 on p."streetID"=s."id"
 WHERE p.age<18 and s."name" LIKE '%Правды%'

8. Вывести упорядоченный по алфавиту список всех улиц с указанием, сколько
жильцов живёт на улице
SELECT 
s."name", count(p."id")
FROM
streets s
LEFT JOIN
persons p 
on s."id"=p."streetID"
GROUP BY s."name"
ORDER BY count(p."id") DESC

9. Вывести список улиц, название которых состоит из 12-ти букв
SELECT 
s."name"
FROM
streets s
WHERE length(s."name")=12

10. Вывести список улиц с количеством жильцов на них меньше 3
SELECT 
s."name", count(p."id")
FROM
streets s
LEFT JOIN
persons p 
on s."id"=p."streetID"
GROUP BY s."name"
HAVING count(p."id")<3